# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (1200, 900)


# Часть 1.
# Написать функции рисования равносторонних геометрических фигур:
# - треугольника
# - квадрата
# - пятиугольника
# - шестиугольника
# Все функции должны принимать 3 параметра:
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Примерный алгоритм внутри функции:
#   # будем рисовать с помощью векторов, каждый следующий - из конечной точки предыдущего
#   текущая_точка = начальная точка
#   для угол_наклона из диапазона от 0 до 360 с шагом XXX
#      # XXX подбирается индивидуально для каждой фигуры
#      составляем вектор из текущая_точка заданной длины с наклоном в угол_наклона
#      рисуем вектор
#      текущая_точка = конечной точке вектора
#
# Использование копи-пасты - обязательно! Даже тем кто уже знает про её пагубность. Для тренировки.
# Как работает копипаста:
#   - одну функцию написали,
#   - копипастим её, меняем название, чуть подправляем код,
#   - копипастим её, меняем название, чуть подправляем код,
#   - и так далее.
# В итоге должен получиться ПОЧТИ одинаковый код в каждой функции

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# sd.line()
# Результат решения см /results/exercise_01_shapes.jpg


# Часть 1-бис.
# Попробуйте прикинуть обьем работы, если нужно будет внести изменения в этот код.
# Скажем, связывать точки не линиями, а дугами. Или двойными линиями. Или рисовать круги в угловых точках. Или...
# А если таких функций не 4, а 44? Код писать не нужно, просто представь объем работы... и запомни это.

# Зачет

# Часть 2 (делается после зачета первой части)
#
# Надо сформировать функцию, параметризированную в местах где была "небольшая правка".
# Это называется "Выделить общую часть алгоритма в отдельную функцию"
# Потом надо изменить функции рисования конкретных фигур - вызывать общую функцию вместо "почти" одинакового кода.
#
# В итоге должно получиться:
#   - одна общая функция со множеством параметров,
#   - все функции отрисовки треугольника/квадрата/етс берут 3 параметра и внутри себя ВЫЗЫВАЮТ общую функцию.
#
# Не забудте в этой общей функции придумать, как устранить разрыв в начальной/конечной точках рисуемой фигуры
# (если он есть. подсказка - на последней итерации можно использовать линию от первой точки)

# Часть 2-бис.
# А теперь - сколько надо работы что бы внести изменения в код? Выгода на лицо :)
# Поэтому среди программистов есть принцип D.R.Y. https://clck.ru/GEsA9
# Будьте ленивыми, не используйте копи-пасту!

# part 1
# point = sd.get_point(200,200)
#
# def triangle(point, angle=0, length=200):
#     x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
#     x1.draw()
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=200, width=10)
#     x2.draw()
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 240, length=200, width=10)
#     x3.draw()
# def triangle(point, angle=0, length=200):
#     if angle < 360:
#         return
#     x1 = sd.get_vector(start_point=point, angle=angle, length=length, width = 10)
#     x1.draw()
#     next_point = x1.end_point
#     next_angle = angle + 120
#     next_length = length
#     triangle(point=next_point, angle= next_angle, length= next_length)
#
#
# point_0 = sd.get_point(300,300)
# triangle(point_0, angle=0, length=200)
# length = 200
# angle=0

# triangle(point=point, angle=0, length=200)
# def square(point, angle=0, length=200):
#     x1 = sd.get_vector(start_point=point,angle=angle,length=length, width=10)
#     x1.draw()
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 90,length=length,width=10)
#     x2.draw()
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 180, length=length, width=10)
#     x3.draw()
#     x4 = sd.get_vector(start_point=x3.end_point, angle= angle + 270, length=length, width=10)
#     x4.draw()

# square(point=point,angle=90,length=250)
# for angle in range(0, 361, 5):
#     square(point=point, angle=angle)

# def pentagon(point,angle=0,length=200):
#     x1 = sd.get_vector(start_point=point,angle=angle,length=length, width=10)
#     x1.draw()
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 72,length=length,width=10)
#     x2.draw()
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 144, length=length, width=10)
#     x3.draw()
#     x4 = sd.get_vector(start_point=x3.end_point, angle= angle + 216, length=length, width=10)
#     x4.draw()
#     x5 = sd.get_vector(start_point=x4.end_point, angle= angle + 288, length=length, width=10)
#     x5.draw()

# pentagon(point=point,angle=0,length=200)
# def hexagon(point, angle=0, length=200):
#     x1 = sd.get_vector(start_point=point,angle=angle,length=length, width=10)
#     x1.draw(sd.COLOR_RED)
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 60,length=length,width=10)
#     x2.draw()
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 120, length=length, width=10)
#     x3.draw()
#     x4 = sd.get_vector(start_point=x3.end_point, angle= angle + 180, length=length, width=10)
#     x4.draw()
#     x5 = sd.get_vector(start_point=x4.end_point, angle= angle + 240, length=length, width=10)
#     x5.draw()
#     x6 = sd.get_vector(start_point=x5.end_point, angle= angle + 300, length=length, width=10)
#     x6.draw()
#
# hexagon(point, angle=0, length=200)


# x = 150
# y = 150
#
# color = sd.random_color()
# for i in range(30):
#     hexagon(point=point, angle=0,length=200)
#     point = sd.get_point(x, y)
#
#     x += 10
#     y += 10

# def triangle(point, angle, length):
#     if angle < 360:
#         return
#     v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=3)
#     v1.draw()
#     next_point = v1.end_point
#     next_angle = angle - 10
#     next_length = length * .75
#     sd.sleep(0.2)
#     triangle(point=next_point, angle=next_angle, length=next_length)

# triangle(point=point, angle=0, length=200)

point = sd.get_point(250,250)
length = 200


def universal_shapes(point,number_of_corners,angle=0):
    if number_of_corners == 3:
        x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
        x1.draw()
        x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=200, width=10)
        x2.draw()
        x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 240, length=200, width=10)
        x3.draw()
    elif number_of_corners == 4:
        x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
        x1.draw()
        x2 = sd.get_vector(start_point=x1.end_point, angle=angle + 90, length=length, width=10)
        x2.draw()
        x3 = sd.get_vector(start_point=x2.end_point, angle=angle + 180, length=length, width=10)
        x3.draw()
        x4 = sd.get_vector(start_point=x3.end_point, angle=angle + 270, length=length, width=10)
        x4.draw()
    elif number_of_corners == 5:
        x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
        x1.draw()
        x2 = sd.get_vector(start_point=x1.end_point, angle=angle + 72, length=length, width=10)
        x2.draw()
        x3 = sd.get_vector(start_point=x2.end_point, angle=angle + 144, length=length, width=10)
        x3.draw()
        x4 = sd.get_vector(start_point=x3.end_point, angle=angle + 216, length=length, width=10)
        x4.draw()
        x5 = sd.get_vector(start_point=x4.end_point, angle=angle + 288, length=length, width=10)
        x5.draw()
    elif number_of_corners == 6:
        x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
        x1.draw(sd.COLOR_RED)
        x2 = sd.get_vector(start_point=x1.end_point, angle=angle + 60, length=length, width=10)
        x2.draw()
        x3 = sd.get_vector(start_point=x2.end_point, angle=angle + 120, length=length, width=10)
        x3.draw()
        x4 = sd.get_vector(start_point=x3.end_point, angle=angle + 180, length=length, width=10)
        x4.draw()
        x5 = sd.get_vector(start_point=x4.end_point, angle=angle + 240, length=length, width=10)
        x5.draw()
        x6 = sd.get_vector(start_point=x5.end_point, angle=angle + 300, length=length, width=10)
        x6.draw()


universal_shapes(point,number_of_corners=5,angle = 0)

sd.pause()

sd.pause()

#TODO ЗАЧЕТ, теперь часть два выделить общую часть всех функций