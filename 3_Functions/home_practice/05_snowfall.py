# -*- coding: utf-8 -*-


# На основе кода из практической части реализовать снегопад:
# - создать списки данных для отрисовки N снежинок
# - нарисовать падение этих N снежинок
# - создать список рандомных длинн лучей снежинок (от 10 до 100) и пусть все снежинки будут разные

N = 20

# Пригодятся функции
# sd.get_point()
# sd.snowflake()
# sd.sleep()
# sd.random_number()
# sd.user_want_exit()


# Примерный алгоритм отрисовки снежинок
#   навсегда
#     очистка экрана
#     для индекс, координата_х из списка координат снежинок
#       получить координата_у по индексу
#       создать точку отрисовки снежинки
#       нарисовать снежинку цветом фона
#       изменить координата_у и запомнить её в списке по индексу
#       создать новую точку отрисовки снежинки
#       нарисовать снежинку на новом месте белым цветом
#     немного поспать
#     если пользователь хочет выйти
#       прервать цикл



# sd.resolution = (1200, 600)
# познакомится с параметрами библиотечной функции рисования снежинки sd.snowflake()

# sd.snowflake(center=point_0, length=200, factor_a=0.5)

# реализовать падение одной снежинки



# while True:
#     sd.clear_screen()
#
# point_x = [200, 205, 210, 215, 220, 225, 230, 235, 240, 245]
# point_y = [400, 405, 410, 415, 420, 425, 430, 435, 440, 445]
#
# for i in point_x:
#     for j in point_y:
#         snowfall = sd.get_point(i,j)
#         sd.snowflake(center=snowfall,length=50, color = sd.COLOR_PURPLE)
#         i_1 = i + 10
#         j_1 = j + 10
#         snowfall_1 = sd.get_point(i_1,j_1)
#         sd.snowflake(center=snowfall_1, length= 40, color = sd.COLOR_WHITE)
#         if j_1 < 50:
#             break
#         sd.sleep(0.1)
#         if sd.user_want_exit():
#             break




# while True:
#     sd.clear_screen()
#
# point_x = [150,200,250,300]
# point_y = [350,400,450,500]
#
# for i in point_y:
#     for j in point_x:
#         snowfall = sd.get_point(i,j)
#         sd.snowflake(center = snowfall, length = 50, color=sd.COLOR_PURPLE)
#         i_1 = i + 10
#         j_1 = j + 10
#         snowfall_1 = sd.get_point(i_1, j_1)
#         sd.snowflake(center=snowfall_1, length = 40, color=sd.COLOR_WHITE)
#         if j_1 < 50:
#             break
#         sd.sleep(0.1)
#         if sd.user_want_exit():
#             break
#
# while True:
#     sd.clear_screen()
#
#     point_x = [50,75,100,125,150]
#     point_y = [400, 425, 450, 475,500]
#
#     for j in point_x:
#         for i in point_y:
#             point_0 = sd.get_point(j,i)
#             sd.snowflake(center=point_0, length=30, color=sd.COLOR_PURPLE)
#             j_1 = j + 50
#             point_1 = sd.get_point(i,j_1)
#             sd.snowflake(center=point_1, length=40, color=sd.COLOR_RED)
#             if j_1 < 50:
#                 break
#             sd.sleep(0.1)
#             if sd.user_want_exit():
#                 break

# y = 500
# x = 100
#
# y2 = 450
# x2 = 150
#
# y3 = 500
# x3 = 150
#
# y4 = 450
# x4 = 100
# while True:
#     sd.clear_screen()
#     point = sd.get_point(x, y)
#     sd.snowflake(center=point, length=50)
#     y -= 10
#     if y < 50:
#        break
#     x = x + 5
#
# sd.sleep(0.2)

#
# while True:
#     sd.clear_screen()
#     x = [50,50,50,50]
#     y = [500,450,350,250,150,50]
#
#     for i in x:
#         for j in y:
#             point_0 = sd.get_point(i,j)
#             sd.snowflake(center=point_0,length=30)
#             x1 = i + 50
#             y1 = j + 100
#             point_1 = sd.get_point(x1,y1)
#             sd.snowflake(center=point_1,length=30)
#             if x1 < 50:
#                 break
#             sd.sleep(0.1)
#             if sd.user_want_exit():
#                 break

# while True:
#     sd.clear_screen()
#     point_x = [50,50,50]
#     point_y = [550,500,450,400,350,300,200,100,50]
#
#     for i in point_y:
#         for j in point_x:
#             point_0 = sd.get_point(j,i)
#             sd.snowflake(center=point_0,length=30)
#             i_1 = i + 50
#
#             point_1 = sd.get_point(j,i_1)
#             sd.snowflake(center=point_1,length=30)
#             if i_1 < 30:
#                 break
#             sd.sleep(0.1)
#             if sd.user_want_exit():
#
#                 break

# x = 50
# y = 500
# while True:
#     sd.clear_screen()
#     point = sd.get_point(x,y)
#     sd.snowflake(center=point,length=30,color=sd.COLOR_PURPLE)
#     y -= 15
#     if y < 50:
#         break
#     x = x + 10
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

# while True:
#     sd.clear_screen()
#     point_x = [50,100,150,200]
#     point_y = [550,500,450,300,250,100]
#     for i in point_x:
#         for j in point_y:
#             point_0 = sd.get_point(i,j)
#             sd.snowflake(center=point_0,length=30)
#             i_1 = i + 50
#             j_1 = j + 50
#             point_1 = sd.get_point(i_1,j_1)
#             sd.snowflake(center=point_1,length=30)
#             if i_1 < 50:
#                 break
#
#             sd.sleep(0.1)
#             if sd.user_want_exit():
#                 break
#
#
# while True:
#     sd.start_drawing()
#     x = [50,100,150,200,300,400,500]
#     y = [500,400,250,100,20]
#
#     for i in x:
#         for j in y:
#             point0=sd.get_point(i,j)
#             sd.snowflake(center=point0,length=30)
#             j -= 10
#             if j < 20:
#                 break
#             i += 5
#             sd.sleep(0.1)
#             if sd.user_want_exit():
#                 break
#     sd.finish_drawing()

# sd.sleep()

# x = 50
# y = 500
# x1 = 75
# y1 = 500
# x2 = 100
# y2 = 500
# x3 = 175
# y3 = 500
#
# while True:
#     sd.clear_screen()
#     point = sd.get_point(x,y)
#     sd.snowflake(center=point,length=30)
#     y -= 10
#     if y < 10:
#         break
#     x += 5
#
#     point1 = sd.get_point(x1,y1)
#     sd.snowflake(center=point1,length=30)
#     y1 -= 10
#     if y1 < 10:
#         break
#     x1 += 5
#
#     point2 = sd.get_point(x2,y2)
#     sd.snowflake(center=point2,length=30)
#     y2 -=5
#     if y2<10:
#         break
#     x += 5
#
#     point3 = sd.get_point(x3,y3)
#     sd.snowflake(center=point3,length=30)
#     y3 -= 10
#     if y3 < 10:
#         break
#     x += 5
#
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

# TODO коммент под кодом. вот так понимаю как нарисовать снежинки, а когда пишу вложенным циклом полцчается то, что отмечу цифрой 2
#
# while True:
#     sd.clear_screen()
#     x = [50,150]
#     y = [500, 450]
#     x1 = [200,250]
#     y1 = [200,100]
#
#     for i in x:
#         for j in y1:
#             point0 = sd.get_point(i,j)
#             sd.snowflake(center=point0,length=30)
#             i1 = i - 10
#             point1 = sd.get_point(i1,j)
#             for ii1 in x1:
#                 for jj1 in y1:
#                     point2 = sd.get_point(ii1,jj1)
#                     sd.snowflake(center=point2,length=30)
#                     jj2 = jj1 - 5
#                     point3 = sd.get_point(ii1,jj2)
#                     sd.snowflake(center=point3,length=30)
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break
# sd.start_drawing()
#
# point_x = [50, 75, 100, 125, 150, 175, 200]
# point_y = [500, 475, 450, 425, 400, 375, 350]
# # point_y = [500, 475, 450, 425, 400, 375, 350, 325, 300, 275, 250, 200, 175, 150, 125, 100, 75, 50, 25, 10]
# while True:
#     for i in point_x:
#
#         for j in point_y:
#             sd.clear_screen()
#             point_0 = sd.get_point(i,j)
#             sd.snowflake(center=point_0,length=30,color=sd.COLOR_PURPLE)
#             i1 = i + 50
#             j1 = j - 150
#             point_1 = sd.get_point(i1,j1)
#             sd.snowflake(center=point_1,length=30,color=sd.COLOR_WHITE)
#             j2 = j1 + 40
#             i2 = i - 100
#             point_2 = sd.get_point(i2,j2)
#             sd.snowflake(center=point_2,length=30,color=sd.COLOR_WHITE)
#             j3 = j + 20
#             i3 = i + 70
#             point_3 = sd.get_point(i3,j3)
#             sd.snowflake(center=point_3,length=30,color=sd.COLOR_WHITE)
#             i4 = i3 + 20
#             j4 = j3 + 80
#             point_4 = sd.get_point(i4,j4)
#             sd.snowflake(center=point_4,length=35,color=sd.COLOR_WHITE)
#             point_5 = sd.get_point(i,j4)
#             sd.snowflake(center=point_5,length=30,color=sd.COLOR_WHITE)
#             point_6 = sd.get_point(i,j4)
#             sd.snowflake(center=point_6,length=30,color=sd.COLOR_WHITE)
#             point_7 = sd.get_point(i,j3)
#             sd.snowflake(center=point_7,length=30,color=sd.COLOR_WHITE)
#             point_8 = sd.get_point(i,j2)
#             sd.snowflake(center=point_8,length=30,color=sd.COLOR_WHITE)
#             point_9 = sd.get_point(i,j4)
#             sd.snowflake(center=point_9,length=30,color=sd.COLOR_WHITE)
#             if j < 50:
#                 break
#             i += 40
#
#             sd.sleep(0.1)
#     if sd.user_want_exit():
#         break
# # sd.finish.drawing()
import simple_draw as sd
sd.resolution = (1200,800)
N = 20
x_coordinate = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]
y_coordinate = [785, 805, 860, 840, 885, 815, 865, 795, 875, 875, 860, 790, 820, 810, 780, 825, 750, 850, 800, 900]
size = [20] * N
speed = [20] * N

while True:
    sd.start_drawing()
    for index, x in enumerate(x_coordinate):
        chage_poz = sd.random_number(-15, 15)
        chage_size = sd.random_number(-1, 1)
        point = sd.get_point(x, y_coordinate[index])
        sd.snowflake(center=point, length=size[index], color=sd.background_color)
        y_coordinate[index] -= speed[index]
        x_x = x_coordinate[index] + chage_poz
        size_in = size[index] + chage_size
        point_2 = sd.get_point(x_x, y_coordinate[index])
        sd.snowflake(center=point_2, length=size_in)
        if y_coordinate[index] < 30:
            y_coordinate[index] += 850
        size[index] += chage_size
        if size[index] > 20:
            if speed[index] < 30:
                speed[index] += 1
        x_coordinate[index] += chage_poz
    sd.finish_drawing()
    sd.sleep(0.1)
    if sd.user_want_exit():
        break

sd.pause()
#TODO не летят они у меня плавно, какие только коорды не пробовал

# Часть 2 (делается после зачета первой части)
#
# Ускорить отрисовку снегопада
# - убрать clear_screen() из цикла
# - в начале рисования всех снежинок вызвать sd.start_drawing()
# - на старом месте снежинки отрисовать её же, но цветом sd.background_color
# - сдвинуть снежинку
# - отрисовать её цветом sd.COLOR_WHITE на новом месте
# - после отрисовки всех снежинок, перед sleep(), вызвать sd.finish_drawing()

# Усложненное задание (делать по желанию)
# - сделать рандомные отклонения вправо/влево при каждом шаге
# - сделать сугоб внизу экрана - если снежинка долетает до низа, оставлять её там,
#   и добавлять новую снежинку

