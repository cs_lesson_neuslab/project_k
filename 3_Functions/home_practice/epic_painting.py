

import simple_draw as sd
sd.resolution=(1200,800)

from fonc_for_epic import draw_branches,grass,rainbow,sun,triangle,window,air_chair
point = sd.get_point(50,200)
draw_branches(point=point,angle=90,length=100)

grass()
rainbow()
sun()
triangle()
window()
air_chair()


sd.pause()