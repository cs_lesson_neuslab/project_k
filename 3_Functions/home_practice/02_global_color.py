# -*- coding: utf-8 -*-
import simple_draw as sd
# sd.background_color = sd.COLOR_BLACK
sd.resolution = (1200, 800)
# Добавить цвет в функции рисования геом. фигур. из упр 01_shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см results/exercise_02_global_color.jpg

# TODO здесь ваш код

# def triangle(point, angle=0, length=200):
#     x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
#     x1.draw(sd.COLOR_BLUE)
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=400, width=10)
#     x2.draw(sd.COLOR_PURPLE)
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 240, length=400, width=10)
#     x3.draw(sd.COLOR_DARK_YELLOW)
#
#
# triangle(point=point, angle=0, length=400)

# color = color_input
# def triangle(point, angle=0, length=200,color=color):
#     x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
#     x1.draw(color=color)
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=200, width=10)
#     x2.draw(color=color)
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 240, length=200, width=10)
#     x3.draw(color=color)

#
# triangle(point, angle=0, length=200, color=color)

# sd.pause()

# print('Выберите цвет фигуры: 1 - красный, 2 - синий, 3 - желтый, 4 - оранжевый, 5 - голубой, 6 - фиолетовый')
# user_input = input('Введите номер цвета: ')
# color_input = int(user_input)
# print('Вы ввели номер цвета', color_input)
# user_color = {1: sd.COLOR_RED, 2: sd.COLOR_BLUE, 3: sd.COLOR_YELLOW, 4: sd.COLOR_ORANGE, 5: sd.COLOR_CYAN,
#                6: sd.COLOR_PURPLE}
# for color_input in user_color:
#     if color_input == 1:
#         color = sd.COLOR_RED
#     elif color_input == 2:
#         color = sd.COLOR_BLUE
#     elif color_input == 3:
#         color = sd.COLOR_YELLOW
#     elif color_input == 4:
#         color == sd.COLOR_ORANGE
#     elif color_input == 5:
#         color = sd.COLOR_CYAN
#     elif color_input == 6:
#         color = sd.COLOR_PURPLE


# def triangle(point, angle=0, length=200,color=color):
#     x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
#     x1.draw(color=color)
#     x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=200, width=10)
#     x2.draw(color=color)
#     x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 240, length=200, width=10)
#     x3.draw(color=color)

# triangle(point, angle=0, length=200, color = color)

# user_color = {1: sd.COLOR_RED, 2: sd.COLOR_BLUE, 3: sd.COLOR_YELLOW, 4: sd.COLOR_ORANGE, 5: sd.COLOR_CYAN,
#                6: sd.COLOR_PURPLE}
# print('Выберите цвет фигуры: 1 - красный, 2 - синий, 3 - желтый, 4 - оранжевый, 5 - голубой, 6 - фиолетовый')
#
# user_input = int(input('Введите номер цвета: '))
# if user_input >= 6:
#     print('Не верный ввод, попробуйте еще раз!')



def triangle(point,color, angle=0, length=200):

    x1 = sd.get_vector(start_point=point, angle=angle, length=length, width=10)
    x1.draw(color=color)
    x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=200, width=10)
    x2.draw(color=color)
    x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 240, length=200, width=10)
    x3.draw(color=color)

# triangle(point, angle=0, length=200, color = color)

user_color = {1: sd.COLOR_RED, 2: sd.COLOR_BLUE, 3: sd.COLOR_YELLOW, 4: sd.COLOR_ORANGE, 5: sd.COLOR_CYAN,
               6: sd.COLOR_PURPLE}
print('Выберите цвет фигуры: 1 - красный, 2 - синий, 3 - желтый, 4 - оранжевый, 5 - голубой, 6 - фиолетовый')
user_input = int(input('Введите номер цвета: '))
print('Вы ввели номер цвета', user_input)
point = sd.get_point(250,250)
triangle(point, angle=0, length = 200, color = user_color[user_input])


sd.pause()

# ЗАЧЕТ