# (определение функций)
import simple_draw as sd

sd.resolution = (1200, 600)


# Написать функцию отрисовки смайлика по заданным координатам
# Форма рожицы-смайлика на ваше усмотрение
# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.
#
#
# def smile(point_x, point_y, color):
#     start_point = sd.get_point(point_x, point_y)
#     sd.circle(start_point, 50, color, width=10)
#     sd.line(sd.get_point(point_x - 20, point_y - 5), sd.get_point(point_x - 10, point_y - 20), color, 5)
#     sd.line(sd.get_point(point_x - 10, point_y - 20), sd.get_point(point_x + 10, point_y - 20), color, 5)
#     sd.line(sd.get_point(point_x + 10, point_y - 20), sd.get_point(point_x + 20, point_y - 5), color, 5)
#     sd.line(sd.get_point(point_x - 15, point_y + 20), sd.get_point(point_x - 15, point_y + 5), color, 5)
#     sd.line(sd.get_point(point_x + 15, point_y + 20), sd.get_point(point_x + 15, point_y + 5), color, 5)

#
# for i in range(10):
#     point = sd.random_point()
#     color = sd.random_color()
#     point_x = point.x
#     point_y = point.y
#     smile(point_x,point_y,color)
#
#
# smile(100,100,color)
#
# def smile(point_x,point_y, color):
#     start_point = sd.get_point(point_x,point_y)
#     sd.circle(start_point,100,color,width=10)
#
# smile(100,100,color=sd.COLOR_WHITE)
def smile(point_x,point_y):

    point = sd.get_point(point_x,point_y)
    point_eye1 = sd.get_point(point_x+30, point_y + 20)
    point_eye_in = sd.get_point(point_x+30,point_y+20)
    point_eye_in1 = sd.get_point(point_x+30,point_y+20)
    point_eye2 = sd.get_point(point_x-40,point_y+20)
    point_eye_in2 = sd.get_point(point_x-40,point_y+20)
    point_ear1 = sd.get_point(point_x+100,point_y+95)
    point_ear2 = sd.get_point(point_x-100,point_y+95)
    point_mouth = sd.get_point(point_x,point_y-40)
    point_mouth_in = sd.get_point(point_x,point_y-40)
    radius = 100
    sd.circle(center_position=point,radius=radius,width=10)
    sd.circle(center_position=point_eye1,radius=30,width=10)
    sd.circle(center_position=point_eye2,radius=20,width=10)
    sd.circle(center_position=point_eye_in,radius=20,width=13,color=sd.COLOR_WHITE)
    sd.circle(center_position=point_eye_in1,radius=10,width=10,color=sd.COLOR_RED)
    sd.circle(center_position=point_eye_in2,radius=10,width=10,color=sd.COLOR_BLACK)
    sd.circle(center_position=point_ear1,radius=45,width=10,color=sd.COLOR_YELLOW)
    sd.circle(center_position=point_ear2,radius=45,width=10,color=sd.COLOR_YELLOW)
    sd.circle(center_position=point_mouth,radius=35,width=20,color=sd.COLOR_RED)
    sd.circle(center_position=point_mouth_in,radius=20,width=20,color=sd.COLOR_WHITE)

for i in range(10):
    point = sd.random_point()
    point_x = point.x
    point_y = point.y
    smile(point_x,point_y)

smile(100,100)


sd.pause()