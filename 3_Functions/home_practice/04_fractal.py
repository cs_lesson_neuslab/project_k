# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1000, 800)


# 1) Написать функцию draw_branches, которая должна рисовать две ветви дерева из начальной точки
# Функция должна принимать параметры:
# - точка начала рисования,
# - угол рисования,
# - длина ветвей,
# Отклонение ветвей от угла рисования принять 30 градусов,

# 2) Сделать draw_branches рекурсивной
# - добавить проверку на длину ветвей, если длина меньше 10 - не рисовать
# - вызывать саму себя 2 раза из точек-концов нарисованных ветвей,
#   с параметром "угол рисования" равным углу только что нарисованной ветви,
#   и параметром "длинна ветвей" в 0.75 меньшей чем длина только что нарисованной ветви

# 3) Запустить вашу рекурсивную функцию, используя следующие параметры:
# root_point = sd.get_point(300, 30)
# draw_branches(start_point=root_point, angle=90, length=100)

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# Возможный результат решения см results/exercise_04_fractal_01.jpg

# можно поиграть -шрифтами- цветами и углами отклонения



# 4) Усложненное задание (делать по желанию)
# - сделать рандомное отклонение угла ветвей в пределах 40% от 30-ти градусов
# - сделать рандомное отклонение длины ветвей в пределах 20% от коэффициента 0.75
# Возможный результат решения см results/exercise_04_fractal_02.jpg

# Пригодятся функции
sd.random_number()
# point = sd.get_point(150,150)
# def draw_branches(point,angle,length):
#     if length < 10:
#         return
#     v1 = sd.get_vector(start_point=point,angle=angle,length=length)
#     v1.draw(color=sd.COLOR_DARK_RED)
#
#     point_0 = v1.end_point
#     angle_0 = angle + 30
#     angle_1 = angle - 30
#     angle_2 = angle + 55
#     length_1 = length * 0.75
#     v2 = sd.get_vector(start_point=point,angle=angle_0,length=length)
#     v2.draw(color = sd.COLOR_DARK_RED, width=5)
#     point_1 = v2.end_point
#     v3 = sd.get_vector(start_point=point_1,angle=angle_2,length=length)
#     v3.draw(color=sd.COLOR_GREEN,width=3)
#     draw_branches(point=point_0, angle=angle_0,length=length_1)
#     draw_branches(point=point_0,angle=angle_1,length=length_1)
    # draw_branches(point=point_1,angle=angle_1,length=length_1)


# point=sd.get_point(550,50)
# draw_branches(point=point,angle=90,length=200)



# def draw_branches(point, angle, length):
#     if length < 10:
#         return
#     v1 = sd.get_vector(start_point=point,angle=angle,length=length,width=20)
#     v1.draw(color=sd.COLOR_DARK_RED)
#     point0 = v1.end_point
#     angle0 = angle + sd.random_number(30,50)
#     length0 = length * 0.8
#     if length0 < 30:
#         v2 = sd.get_vector(start_point=point0,angle=angle0,length=length0,width=3)
#         v2.draw(color=sd.COLOR_DARK_RED)
#         point1 = v2.end_point
#         v3 = sd.get_vector(start_point=point1,angle=angle0,length=length0,width=3)
#         v3.draw(color=sd.COLOR_GREEN)
#         draw_branches(point=point0,angle=angle0,length=length0)
#         draw_branches(point=point1,angle=angle0,length=length0)
#
#
#
#
# draw_branches(point=point,angle=90,length=200)
point = sd.get_point(550,50)
def draw_branches(point,angle,length):
    if length < 10:
        return
    v1 = sd.get_vector(start_point=point,angle=angle,length=length,width = 6)
    v1.draw(color=sd.COLOR_DARK_RED)
    point_0 = v1.end_point
    angle_0 = angle + 30
    length_0 = length * 0,8
    if length < 30:
        v2 = sd.get_vector(start_point=point,angle=angle_0,length=length)
        v2.draw(color=sd.COLOR_GREEN)
        draw_branches(point=point_0,angle=angle,length=length)
        draw_branches(point=point_0,angle=angle_0,length=length_0)



draw_branches(point=point,angle=90,length=200)
sd.pause()
#TODO Не вижу правильного решения, видимо не запушил
#TODO дерево
