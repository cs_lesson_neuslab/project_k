import simple_draw as sd
sd.resolution = (1200, 800)
# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана
#
# Код функций из упр 02_global_color.py скопировать сюда
# Результат решения см results/exercise_03_shape_select.jpg


# user_figure = {1: triangle, 2: square, 3: pentagon, 4: hexagon}



user_color = {1: sd.COLOR_RED, 2: sd.COLOR_BLUE, 3: sd.COLOR_YELLOW, 4: sd.COLOR_ORANGE, 5: sd.COLOR_CYAN,
               6: sd.COLOR_PURPLE}
def triangle(point,color, width = 5, angle=0, length=200):

    x1 = sd.get_vector(start_point=point, angle=angle, length=length)
    x1.draw(color=color)
    x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 120, length=200)
    x2.draw(color=color)
    x3 = sd.get_vector(start_point=x2.end_point,  angle= angle + 240, length=200)
    x3.draw(color=color)
def square(point, color, width = 5, angle=0, length=200):
    x1 = sd.get_vector(start_point=point,angle=angle,length=length)
    x1.draw(color=color)
    x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 90,length=length)
    x2.draw(color=color)
    x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 180, length=length)
    x3.draw(color=color)
    x4 = sd.get_vector(start_point=x3.end_point, angle= angle + 270, length=length)
    x4.draw(color=color)

def pentagon(point,color,width = 5,angle=0,length=200):
    x1 = sd.get_vector(start_point=point,angle=angle,length=length)
    x1.draw(color=color)
    x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 72,length=length)
    x2.draw(color=color)
    x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 144, length=length)
    x3.draw(color=color)
    x4 = sd.get_vector(start_point=x3.end_point, angle= angle + 216, length=length)
    x4.draw(color=color)
    x5 = sd.get_vector(start_point=x4.end_point, angle= angle + 288, length=length)
    x5.draw(color=color)
def hexagon(point,color, width = 5, angle=0, length=200):
    x1 = sd.get_vector(start_point=point,angle=angle,length=length)
    x1.draw(color=color)
    x2 = sd.get_vector(start_point=x1.end_point, angle= angle + 60,length=length)
    x2.draw(color=color)
    x3 = sd.get_vector(start_point=x2.end_point, angle= angle + 120, length=length)
    x3.draw(color=color)
    x4 = sd.get_vector(start_point=x3.end_point, angle= angle + 180, length=length)
    x4.draw(color=color)
    x5 = sd.get_vector(start_point=x4.end_point, angle= angle + 240, length=length)
    x5.draw(color=color)
    x6 = sd.get_vector(start_point=x5.end_point, angle= angle + 300, length=length)
    x6.draw(color=color)
while True:
    print('Выберите цвет фигуры: 1 - красный, 2 - синий, 3 - желтый, 4 - оранжевый, 5 - голубой, 6 - фиолетовый')
    user_input = int(input('Введите номер цвета: '))
    result = int(user_input)
    if result > 0 and result < 7:
        print('Вы ввели номер цвета', user_input)
        break
    else:
        print('Я просил ввести от 1 до 6, а вы ввели', result,'Попробуйте еще раз')
while True:
    print('Введите номер фигуры: 1 - треугольник, 2 - квадрат, 3 - пятиугольник, 4 - шестиугольник')
    figure_input = int(input('Введите номер фигуры: '))
    result2 = int(figure_input)
    if result2 > 0 and result2 < 5:
        print('Вы ввели номер фигуры:', result2)
        break
    else:
        print('Я просил ввести от 1 до 4, а вы ввели',result2,'Попробуйте еще раз')
point = sd.get_point(350, 350)
if figure_input == 1:
    triangle(point, angle=0, length=200, color=user_color[user_input])
elif figure_input == 2:
    square(point, angle=0, length=200, color=user_color[user_input])
elif figure_input == 3:
    pentagon(point, angle=0, length=200, color=user_color[user_input])
elif figure_input == 4:
    hexagon(point, angle=0, length=200, color=user_color[user_input])

user_figure = {1: triangle, 2: square, 3: pentagon, 4: hexagon}








sd.pause()


# ЗАЧЕТ
#TODO не обязательная доработка, отсутвует защита от "дурака" на каждом этапе выбора
#TODO Сделал, можешь проверять =)