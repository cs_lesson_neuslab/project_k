import simple_draw as sd

sd.resolution = (1200, 1000)

# point_x = [50, 100, 150, 200, 250, 300, 350]
# point_y = [510, 480, 499, 530, 450, 500, 490]


# while True:
#     sd.start_drawing()
#     for j, i in enumerate(point_x):
#         point_0 = sd.get_point(i, point_y[j])
#         sd.snowflake(center=point_0, length=20, color=sd.background_color)
#         point_y[j] -= 30
#         point_1 = sd.get_point(i, point_y[j])
#         sd.snowflake(center=point_1, length=20, color=sd.COLOR_WHITE)
#         if point_y[j] < 30:
#             point_y[j] += 500
#         sd.finish_drawing()
#         sd.sleep(0.1)
    # for i,j  in enumerate(point_y):
    #     sd.clear_screen()
    #     point_2 = sd.get_point(i,point_x[i])
    #     sd.snowflake(center=point_2,length=20,color=sd.background_color)
    #     point_x[i] -= sd.random_number(10,20)
    #     point_3 = sd.get_point(i,point_x[i])
    #     sd.snowflake(center=point_3,length=20,color=sd.COLOR_WHITE)
    #     if point_x[i] < 750:
    #         point_x[i] += 250


    # if sd.user_want_exit():
    #     break

# sd.finish.drawing()
# sd.pause()


N = 20
x_coordinate = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000]
y_coordinate = [785, 805, 860, 840, 885, 815, 865, 795, 875, 875, 860, 790, 820, 810, 780, 825, 750, 850, 800, 900]
size = [20] * N
speed = [20] * N

while True:
    sd.start_drawing()
    for index, x in enumerate(x_coordinate):
        chage_poz = sd.random_number(-15, 15)
        chage_size = sd.random_number(-1, 1)
        point = sd.get_point(x, y_coordinate[index])
        sd.snowflake(center=point, length=size[index], color=sd.background_color)
        y_coordinate[index] -= speed[index]
        x_x = x_coordinate[index] + chage_poz
        size_in = size[index] + chage_size
        point_2 = sd.get_point(x_x, y_coordinate[index])
        sd.snowflake(center=point_2, length=size_in)
        if y_coordinate[index] < 30:
            y_coordinate[index] += 850
        size[index] += chage_size
        if size[index] > 20:
            if speed[index] < 30:
                speed[index] += 1
        x_coordinate[index] += chage_poz
    sd.finish_drawing()
    sd.sleep(0.1)
    if sd.user_want_exit():
        break

sd.pause()