# Memoization – свойство функций сохранять (кешировать) результаты вычислений, дабы не вычислять в последствии повторно.
# Эта технология оптимизации позволят достичь прироста скорости работы за счет потерь в свободной памяти.
# Допустим, у нас есть некая функция bigfunc, результат которой зависят только от переданных в нее аргументов,
# а сложность вычислений достаточно большая.
# Естественно нам не хотелось бы производить вычисления при каждом вызове bigfunc
# если она уже вызывалась ранее с теми же параметрами.

import time
from typing import Dict

def factorial(n):
    if n == 1:
        return 1
    factorial_n_minus_1 = factorial(n=n - 1)
    return n * factorial_n_minus_1


print(time.process_time(), factorial(998))


# factorial_memo = {}
# def factorial(k):
#     if k < 2:
#         return 1
#     if k not in factorial_memo:
#         factorial_memo[k] = k * factorial(k-1)
#         return factorial_memo[k]
#
# print(time.process_time())
# print(factorial(900))




# def fib(n):
#     if n < 2:
#         return n
#     return fib(n - 2) + fib(n - 1)
#
#
# print('fib(20) =', fib(36))

_fib_cache = {1: 1, 2: 1}  # ключ - номер числа, значения - число Фибоначчи

def mem_fib(n):
    result = _fib_cache.get(n)
    print(result)
    if result is None:
        result = mem_fib(n-2) + mem_fib(n-1)
        _fib_cache[n] = result
    return result

print('mem_fib(200) =', mem_fib(100))
print(time.process_time())

