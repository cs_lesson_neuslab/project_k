# -*- coding: utf-8 -*-
#
import simple_draw as sd

sd.resolution = (1200, 800)

# # нарисовать ветку дерева из точки (300, 5) вертикально вверх длиной 100


# # сделать функцию рисования ветки из заданной точки,
# заданной длины, с заданным наклоном

# def branch(point, angle, length):
#     v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=3)
#     v1.draw()
#     return v1.end_point


# point_0 = sd.get_point(600, 5)
# angle_0 = 90
# length_0 = 200
# next_point = branch(point=point_0, angle=angle_0, length=length_0)
# next_angle = angle_0 - 30
# next_length = length_0 * .75
# next_point = branch(point=next_point, angle=next_angle, length=next_length)
# next_angle = next_angle - 30
# next_length = next_length * .75
# next_point = branch(point=next_point, angle=next_angle, length=next_length)
#
# # написать цикл рисования ветвей с постоянным уменьшением длины на 25% и отклонением на 30 градусов
# point_0 = sd.get_point(600, 5)
# angle_0 = 90
# length_0 = 200
# next_angle = angle_0
# next_length = length_0
# next_point = point_0
# while next_length > 1:
#     next_point = branch(point=next_point, angle=next_angle, length=next_length)
#     next_angle = next_angle - 30
#     next_length = next_length * .75
#     sd.sleep(0.1)
#
# for y in range(5, 100, 40):
#     point_0 = sd.get_point(300, y)
#     angle_0 = 90
#     length_0 = 200
#     next_angle = angle_0
#     next_length = length_0
#     next_point = point_0
#     while next_length > 1:
#         next_point = branch(point=next_point, angle=next_angle, length=next_length)
#         next_angle = next_angle - 30
#         next_length = next_length * .75

point = sd.get_point(550,50)

def draw_branches(point,angle,length):
    if length < 10:
        return
    v1 = sd.get_vector(start_point=point,angle=angle,length=length,width=6)
    v1.draw(color=sd.COLOR_DARK_RED)
    point0 = v1.end_point
    angle0 = angle + sd.random_number(30,50)
    length_1=length * 0,8
    if length_1 < 20:
        v2 = sd.get_vector(start_point=point0,angle=angle0,length=length,width=3)
        v2.draw(color=sd.COLOR_GREEN)
        v3 = sd.get_vector(start_point=point0,angle=angle0,length=length_1,width=3)
        v3.draw(sd.COLOR_GREEN)
    draw_branches(point=point0,angle=angle0,length=length_1)
    draw_branches(point=point0,angle=angle0,length=length_1)


draw_branches(point=point,angle=90,length=100)
