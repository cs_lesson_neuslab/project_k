# -*- coding: utf-8 -*-

# pip install simple_draw

import simple_draw as sd
sd.resolution = (1200, 1000)

# нарисовать треугольник из точки (300, 300) с длиной стороны 200

point = sd.get_point(300, 300)
length = 200

# v1 = sd.get_vector(start_point=point, angle=0, length=200, width=3)
# v1.draw()
#
# v2 = sd.get_vector(start_point=v1.end_point, angle=120, length=200, width=3)
# v2.draw()
# # #
# v3 = sd.get_vector(start_point=v2.end_point, angle=240, length=200, width=3)
# v3.draw()

#
def triangle(point, angle=0):
    v1 = sd.get_vector(start_point=point, angle=angle, length=200, width=3)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 120, length=200, width=3)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 240, length=200, width=3)
    v3.draw()



point_0 = sd.get_point(600, 500)
triangle(point=point_0, angle=50)
# #



sd.pause()

