# Простое определение функции

# def <имя_функции>(...):
#     <блок кода>


def some_func():
    print('Привет! Я функция')


some_func()

my_list = [3, 14, 15, 92, 6]
for element in my_list:
    some_func()


# Более сложное определение функции - параметры

def func_with_params(param):
    print('Функцию вызвали с парамтером', param)


my_list = [3, 14, 15, 92, 6]
for element in my_list:
    print('Начало цикла')
    func_with_params(element)
    print('Конец цикла')

for element in my_list:
    print('Начало цикла')
    func_with_params(param=element)
    print('Конец цикла')


# возврат значения из функции

def power(number, pow):
    print('Функцию вызвали с параметрами', number, pow)
    power_value = number ** pow
    return power_value


my_list = [3, 14, 15, 92, 6]
for element in my_list:
    result = power(element, 10)
    print(result)

for element in my_list:
    result = power(number=element, pow=element)
    print(result)


# если нет return то возвращается None

def some_func():
    print("я ничего не верну")

result = some_func()
print(result)


# можно возвращать несколько значений
def create_default_user():
    name = "Василий"
    age = 27
    return name, age
