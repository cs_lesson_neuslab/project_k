
# Написать функцию, которая вычисляет среднее арифметическое элементов массива, переданного ей в качестве аргумента.
# num = int(input('Введите несколько чисел:'))
def sum_medium(num):
    myStr = str(num)
    lst_num = [int(i) for i in myStr]
    mid_sum = sum(lst_num)/len(myStr)
    print(mid_sum)

# sum_medium(num)


# Дан одномерный массив, состоящий из натуральных чисел. Выполнить сортировку данного массива
# по возрастанию суммы цифр чисел. Например, дан массив чисел [14, 30, 103].
# После сортировки он будет таким: [30, 103, 14], так как сумма цифр числа 30 составляет 3,
# числа 103 равна 4, числа 14 равна 5.
#
# Вывести на экран исходный массив, отсортированный массив,
# а также для контроля сумму цифр каждого числа отсортированного массива.

lst = [14, 30, 103]
lst_str = str(lst)
myList = []
for line in lst_str:
    myList.append(line.split())
print(myList)


# Создать функцию calc(a, b, operation). Описание входных параметров:
# 1. Первое число
# 2. Второе число
# 3. Действие над ними:
#    1) + Сложить
#    2) - Вычесть
#    3) * Умножить
#    4) / Разделить
#    5) В остальных случаях функция должна возвращать "Операция не поддерживается"
def calc(operation):
    a = int(input('Введите число:'))
    b = int(input('Введите второе число:'))
    if operation == '+':
        print(a+b)
    elif operation == '-':
        print(a-b)
    elif operation == '*':
        print(a*b)
    elif operation == '/':
        print(a/b)
    else:
        print('Операция не поддерживается')

# TODO ниже код

# Написать функцию square, принимающую 1 аргумент — сторону квадрата,
# и возвращающую 3 значения (с помощью кортежа): периметр квадрата, площадь квадрата и диагональ квадрата.

# TODO ниже код

# Написать функцию month_to_season(), которая принимает 1 аргумент - номер месяца - и возвращает название сезона,
# к которому относится этот месяц.
# Например, передаем 2, на выходе получаем 'Зима'.
def month_to_season(number_of_month):
    if number_of_month == 12 or number_of_month == 1 or number_of_month == 2:
        print('winter')
    elif number_of_month == 3 or number_of_month == 4 or number_of_month == 5:
        print('spring')
    elif number_of_month == 6 or number_of_month == 7 or number_of_month == 8:
        print('summer')
    elif number_of_month == 9 or number_of_month == 10 or number_of_month == 11:
        print('autumn')
# month_to_season(12)


# TODO ниже код

# Пользователь делает вклад в размере a рублей сроком на years лет под 10% годовых
# (каждый год размер его вклада увеличивается на 10%.
# Эти деньги прибавляются к сумме вклада, и на них в следующем году тоже будут проценты).
# Написать функцию bank, принимающая аргументы a и years, и возвращающую сумму, которая будет на счету пользователя.

# TODO ниже код

# Написать функцию is_prime, принимающую 1 аргумент — число от 0 до 1000,
# и возвращающую True, если оно простое, и False - иначе.

# TODO ниже код

