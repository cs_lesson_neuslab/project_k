# (цикл for)
import simple_draw as sd
sd.resolution = (1200, 800)
# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for


# Подсказки:
#  Для отрисовки кирпича использовать функцию rectangle
#  Алгоритм должен получиться приблизительно такой:
#
#   цикл по координате Y
#       вычисляем сдвиг ряда кирпичей
#       цикл координате X
#           вычисляем правый нижний и левый верхний углы кирпича
#           рисуем кирпич
#

# x = 600
# y = 200
# width = 100
# height = 50
# for y in range(y, 300, 50):
#     x = int(y/50) % 2 * 50
#     for x in range(x, 500, 100):
#         x1_point = sd.get_point(x, y)
#         y1_point = sd.get_point(x + width, y + height)
#         sd.rectangle(left_bottom=x1_point, right_top=y1_point, color = sd.COLOR_YELLOW, width=10)

# #

left_bottom = 0
right_top = 50
for wall in range(6):
    left_top = 0
    for right_bottom in range(100, 700, 100):
        a = sd.get_point(left_top, left_bottom)
        b = sd.get_point(right_bottom, right_top)
        sd.rectangle(a, b, width=2)
        right_bottom = 150
        left_top = 50
        left_bottom += 50
        right_top += 50
        for _ in range(5):
                a = sd.get_point(left_top, left_bottom)
                b = sd.get_point(right_bottom, right_top)
                sd.rectangle(a, b, width=2)
                right_bottom += 100
                left_bottom += 50
                right_top += 50

sd.pause()