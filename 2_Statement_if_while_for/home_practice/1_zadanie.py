# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
# user_input = input("Введите, пожалуйста, номер месяца: ")
# month = int(user_input)
# print('Вы ввели', month)

# user_input = input("Введите, пожалуйста, номер месяца: ")
# if user_input == '01':
#     print('31 days')
# elif user_input == '02':
#     print('28 days')
# elif user_input == '03':
#     print('31 days')
# elif user_input == '04':
#     print('30 days')
# elif user_input == '05':
#     print('31 days')
# elif user_input == '06':
#     print('30 days')
# elif user_input == '07':
#     print('31 days')
# elif user_input == '08':
#     print('31 days')
# elif user_input == '09':
#     print('30 days')
# elif user_input == '10':
#     print('31 days')
# elif user_input == '11':
#     print('30 days')
# elif user_input == '12':
#     print('31 days')
# else:
#     print('не корректный ввод')



user_input = input("Введите, пожалуйста, номер месяца: ")
if user_input in ['01','03','05','07','08','10','12']:
    print('31 days')
elif user_input in ['04','06','09','11']:
    print('30 days')
elif user_input == '02':
    print('28 days')
else:
    print('Не корректный ввод')




#TODO лучше но нет, избыточный код. Подсказка 1.  подумай какой еще тип данных можно применить!

# months = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
# for i in months:
#     if i == 3 or i == 4 or i == 5:
#         print('Весна')
#     elif i == 6 or i == 7 or i == 8:
#         print('Лето')
#     elif i == 9 or i == 10 or i == 11:
#         print('Осень')
#     elif i == 12 or i == 1 or i == 2:
#         print('Зима')
#     else:
#         print('Не верный ввод!')

# user_input = input("Введите, пожалуйста, номер месяца: ")
# month = int(user_input)
# print('Вы ввели', month)
#
# days_in_month = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30,
#               7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}
#
# if month in days_in_month:
#     print(f'Дней в месяце {days_in_month[month]}')
# else:
#     print('Ошибка, такого месяца нет')