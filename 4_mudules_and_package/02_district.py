# -*- coding: utf-8 -*-

# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join

from district.central_street.house1 import room1,room2
a = room1.folks + room2.folks
# print('На районе живут:',room1.folks,room2.folks,',')
# print(' '.join(a))
from district.central_street.house2 import room1, room2

# print(room1.folks,',',room2.folks,',')
b = a + room1.folks + room2.folks
# print(' '.join(b))
from district.soviet_street.house1 import room1,room2
# print(room1.folks,',',room2.folks,',')
c = b + room1.folks + room2.folks
# print(' '.join(c))
from district.soviet_street.house2 import room1,room2
# print(room1.folks,',',room2.folks)
d = c + room1.folks + room2.folks
print('На районе живут:',' '.join(d))

