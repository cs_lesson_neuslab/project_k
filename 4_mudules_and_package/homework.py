x = 1
y = 1
z = 1
n = 2
# 5
# 2 3 6 6 5
# answer = [[i, j, k] for i in range(x + 1) for j in range(y + 1) for k in range(z + 1) if i + j + k != n]
# print(answer)

# a = "this is a string"
# >>> a = a.split(" ") # a is converted to a list of strings.
# >>> print a
# ['this', 'is', 'a', 'string']
# # string = "abracadabra"

string = "abracadabra"
# l = list(string)
# l[5] = 'k'
# string = ''.join(l)
# print (string)


