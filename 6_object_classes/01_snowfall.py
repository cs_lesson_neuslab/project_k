# -*- coding: utf-8 -*-
import random
import simple_draw as sd

# Шаг 1: Реализовать падение снежинки через класс. Внести в методы:
#  - создание снежинки с нужными параметрами
#  - отработку изменений координат
#  - отрисовку
x_coordinate = None
y_coordinate = None

quantity = 25

class Snowflake:
    def coord(quantity):
        global x_coordinate
        global y_coordinate
        coordinates = {a: random.randrange(600, 900, 15) for a in range(50, 50 * quantity, 50)}
        print(coordinates)
        x_coordinate = list(coordinates.keys())
        y_coordinate = list(coordinates.values())



def make_snow(color = sd.COLOR_WHITE):
    for index, x in enumerate(x_coordinate):
        point = sd.get_point(x, y_coordinate[index])
        sd.snowflake(center=point,length=20,color=color)

def change_snow():
    global x_coordinate
    global y_coordinate
    global change_poz
    for index, x in enumerate(x_coordinate):
        change_poz = sd.random_number(-30,30)
        y_coordinate[index] -= 20
        x_coordinate[index] += change_poz

list_num = []
def snow_again(list_num):
    for index in list_num:
        y_coordinate[index] += 950
        # print(list_num)
        return list_num

def chek_snow():
    check_num = []
    for index, y in enumerate(y_coordinate):
        if y < 30:
            check_num.append(index)
    return check_num

check_num = []
def del_snow(check_num):
    for index in check_num:
        check_num.remove(index)
        print(check_num)
        return check_num


        coord(25)
        make_snow(color=sd.COLOR_WHITE)
        change_snow()
        snow_again()
        chek_snow()
        del_snow()

snow = Snowflake()

snow()


# flake = Snowflake()

# while True:
#     flake.clear_previous_picture()
#     flake.move()
#     flake.draw()
#     if not flake.can_fall():
#         break
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

# шаг 2: создать снегопад - список объектов Снежинка в отдельном списке, обработку примерно так:

# flakes = get_flakes(count=N)  # создать список снежинок
# while True:
#     for flake in flakes:
#         flake.clear_previous_picture()
#         flake.move()
#         flake.draw()
#     fallen_flakes = get_fallen_flakes()  # подчитать сколько снежинок уже упало
#     if fallen_flakes:
#         append_flakes(count=fallen_flakes)  # добавить еще сверху
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

sd.pause()
