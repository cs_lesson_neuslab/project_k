# -*- coding: utf-8 -*-

from random import randint
from termcolor import cprint
# Доработать практическую часть урока lesson_007/python_snippets/08_practice.py

# Необходимо создать класс кота. У кота есть аттрибуты - сытость и дом (в котором он живет).
# Кот живет с человеком в доме.
# Для кота дом характеризируется - миской для еды и грязью.
# Изначально в доме нет еды для кота и нет грязи.

# Доработать класс человека, добавив методы
#   подобрать кота - у кота появляется дом.
#   купить коту еды - кошачья еда в доме увеличивается на 50, деньги уменьшаются на 50.
#   убраться в доме - степень грязи в доме уменьшается на 100, сытость у человека уменьшается на 20.
# Увеличить кол-во зарабатываемых человеком денег до 150 (он выучил пайтон и устроился на хорошую работу :)

# Кот может есть, спать и драть обои - необходимо реализовать соответствующие методы.
# Когда кот спит - сытость уменьшается на 10
# Когда кот ест - сытость увеличивается на 20, кошачья еда в доме уменьшается на 10.
# Когда кот дерет обои - сытость уменьшается на 10, степень грязи в доме увеличивается на 5
# Если степень сытости < 0, кот умирает.
# Так же надо реализовать метод "действуй" для кота, в котором он принимает решение
# что будет делать сегодня

# Человеку и коту надо вместе прожить 365 дней.

class Cat:

    def __init__(self,name):
        self.fullness = 50
        self.name = name
        self.house = None
        self.power = 50

    def __str__(self):
        return 'Я - {}, энергия {}, еда {}'.format(self.name,self.power, self.fullness)

    def go_to_the_house(self, house):
        self.house = house
        self.fullness -= 10
        cprint('{} Вьехал в дом'.format(self.name), color='cyan')

    def eat(self):
        if self.house.food >= 10:
            cprint('{} поел'.format(self.name), color='yellow')
            self.fullness += 10
            self.house.food -= 10
            self.house.clean -= 10
            self.power += 10
        else:
            cprint('{} нет еды'.format(self.name), color='red')

    def destroyer(self):
        cprint('{} весь день драл диван'.format(self.name), color='blue')
        self.fullness -= 10
        self.power -= 5
        self.house.clean -= 10

    def sleep(self):
        cprint('{} ложится спать, затихни кожаный мешок'.format(self.name), color='blue')
        self.fullness -= 10
        self.power += 10

    def lick(self):
        self.house.clean += 20
        self.power -= 10

    def cry(self):
        self.house.food += 20
        self.power -= 5

    def death(self):
        # if self.fullness <= 0:
        self.house.clean -= 30
        cprint('{} УМЕР!!!'.format(self.name), color='red')


    def act(self):
        global dice
        dice = randint(1, 6)
        if self.fullness <= 0:
            self.death()
            return
        elif self.fullness < 20:
            self.eat()
        elif self.house.food < 10:
            self.cry()
        elif self.power < 10:
            self.sleep()
        elif self.house.clean < 20:
            self.lick()
        elif dice == 1:
            self.destroyer()
        elif dice == 2:
            self.eat()
        elif dice == 3:
            self.lick()
        elif dice == 4:
            self.cry()
        elif dice == 5:
            self.death()
            return
        else:
            self.destroyer()

class House:

    def __init__(self, food):
        self.food = food
        self.clean = 50

    def __str__(self):
        return 'В доме еды осталось {}'.format(
                self.food)


cats = [
    Cat(name = 'Сема'),
    Cat(name = 'Плинтус'),
    Cat(name = 'Ошибка_Выжевшего')
]


my_sweet_home = House(50)
for i in cats:
    i.go_to_the_house(house=my_sweet_home)

for day in range(1, 366):
    print('================ день {} =================='.format(day))
    for i in cats:
        i.act()


    print('--- в конце дня ---')
    for i in cats:
        print(i)
    print(my_sweet_home)




#TODO Дим, я поковырял этот код  Дополнил. я знаешь что не смог, если в act добавить рандомно выполнение функции смерти, как этого кота убрать из дальнейшего выполнения

    # def __str__(self):
    #     return 'В доме еды осталось {}, денег осталось {}'.format(
    #             self.food, self.money)
    #
    #


    # def house(self,food, dirty):
    #     self.food = food
    #     self.dirty = dirty
    #
    # def eating_time(self):
    #     if self.fullness >= 20:
    #         cprint('{} поел'.format(self.name), color='yellow')
    #         self.fullness += 20
    #         self.house.food -=10
    #
    # def sleep(self):
    #     cprint('{} валялся на диване целый день'.format(self.name), color='green')
    #     self.fullness -= 10
    #     self.house.dirty += 5
    #     self.power += 10
    #
    #
    # def destroyer(self):
    #     cprint('{} драл спинку дивана целый день'.format(self.name), color='green')
    #     self.fullness -= 15
    #     self.house.dirty += 10
    #     self.power -= 20
    #
    # def cry(self):
    #     if self.house.food < 20:
    #         cprint('{} ходит и орет просит корм'.format(self.name), color='red')
    #         self.house.food += 50
    #
    # def death(self):
    #     if self.fullness < 0:
    #         cprint('{} умер...'.format(self.name), color='red')
    #         self.house.dirty += 20
    #
    #










# Усложненное задание (делать по желанию)
# Создать несколько (2-3) котов и подселить их в дом к человеку.
# Им всем вместе так же надо прожить 365 дней.

# (Можно определить критическое количество котов, которое может прокормить человек...)
