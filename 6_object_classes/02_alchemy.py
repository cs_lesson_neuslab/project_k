from termcolor import cprint

# -*- coding: utf-8 -*-


# Создать прототип игры Алхимия: при соединении двух элементов получается новый.
# Реализовать следующие элементы: Вода, Воздух, Огонь, Земля, Шторм, Пар, Грязь, Молния, Пыль, Лава.
# Каждый элемент организовать как отдельный класс.
# Таблица преобразований:
#   Вода + Воздух = Шторм
#   Вода + Огонь = Пар
#   Вода + Земля = Грязь
#   Воздух + Огонь = Молния
#   Воздух + Земля = Пыль
#   Огонь + Земля = Лава

# Сложение элементов реализовывать через __add__
# Если результат не определен - то возвращать None
# Вывод элемента на консоль реализовывать через __str__
#
# Примеры преобразований:
#   print(Water(), '+', Air(), '=', Water() + Air())
#   print(Fire(), '+', Air(), '=', Fire() + Air())

class Water:
    def __init__(self):
        self.name = 'Вода'
        print('Это - {}'.format(self.name))


class Fire:
    def __init__(self):
        self.name = 'Огонь'
        print('Это - {}'.format(self.name))

class Steam:
    def __init__(self):
        self.name = 'Пар'
        print('Это - {}'.format(self.name))

class Air:
    def __init__(self):
        self.name = 'Воздух'
        print('Это - {}'.format(self.name))

class Earth:
    def __init__(self):
        self.name = 'Земля'
        print('Это - {}'.format(self.name))

class Storm:
    def __init__(self):
        self.name = 'Шторм'
        print('Это - {}'.format(self.name))

class Dirty:
    def __init__(self):
        self.name = 'Грязь'
        print('Это - {}'.format(self.name))

class Flash:
    def __init__(self):
        self.name = 'Молния'
        print('Это - {}'.format(self.name))

class Dust:
    def __init__(self):
        self.name = 'Пыль'
        print('Это - {}'.format(self.name))

class Lava:
    def __init__(self):
        self.name = 'Лава'
        print('Это - {}'.format(self.name))

    def __add__(self):
        if Water() + Air():
            return Storm()
        elif Water() + Fire():
            return Steam()
        elif Water + Earth():
            return Dirty()
        elif Air() + Fire():
            return Flash()
        elif Air() + Earth():
            return Dust()
        elif Fire() + Earth():
            return Lava()
        else:
            return None

    def __str__(self):
        print('{} '+' {} = {}'.format(
            self.name, self.name))


    def act(self):
        print('у тебя есть 10 элементов:'
              'Вода, Огонь, Пар, Воздух, Земля, Шторм,'
              'Грязь, Молния, Пыль, Лава')
        print('Попробуй подобрать два элемента, что бы появился третий:')
        print(input('Введите названия двух элементов:'))















# Усложненное задание (делать по желанию)
# Добавить еще элемент в игру.
# Придумать что будет при сложении существующих элементов с новым.
