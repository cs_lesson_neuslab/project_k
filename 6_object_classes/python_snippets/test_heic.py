import os
from PIL import Image,ImageFont,ImageDraw
from PIL.ImageFont import truetype
from tkinter import Tk, font


def heic_print(directory):
    for file in os.listdir(directory):
        print(file)
        if file.endswith(".jpg"):
            name = os.path.splitext(file)[0].replace("-", ' ').title()
            image = Image.open(directory + "/" + file)
            image.load()
            font = truetype('Avenir', size=55)
            draw_text = ImageDraw.Draw(image)
            draw_text.text((image.size[0] - 400, image.size[1] - 50), name, fill='red', font=font)
            save_folder = "/Users/begemot/Desktop/new_folder"
            image.save(save_folder + "/" + file)
            image.show()

directory = input(":")
print(directory)
heic_print(directory)