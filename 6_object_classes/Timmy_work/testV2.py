from bs4 import BeautifulSoup
import requests

url = "https://www.airbnb.ru/"
request = requests.get(url)
soup = BeautifulSoup(request.text, "html.parser")
theme = soup.find_all("td", class_="title")
for themes in theme:
    themes = themes.find("a", {'class':'storylink'})
    if themes is not None:
        sublink = themes.get('href')
        print(str(themes.text)+ " "+ str(sublink))
        print = "==="