import os
from PIL import Image, ImageDraw

directory = "/Users/begemot/PycharmProjects/project_k/6_object_classes/Timmy_work"
for file in os.listdir(directory):
    if file.endswith(".jpg"):
        direct = os.path.abspath(file)
        print(direct)
        name = os.path.splitext(file)[0].replace('-', ' ').title()
        print(name)
        pic = Image.open(direct)
        draw_text = ImageDraw.Draw(pic)
        draw_text.text((930, 650), name, fill='white')
