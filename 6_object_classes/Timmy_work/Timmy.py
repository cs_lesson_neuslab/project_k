from itertools import permutations


def find(number,A):
    """ищет х в А и возвращает True если такой есть
    False если такого нет"""
    flag = False
    for x in A:
        if number == x:
            flag = True
            break
    return flag


def gen_permutations(N:int, M:int=-1, prefix=None):
    """Генерация всех перестановок N чисел в M позициях, с перфиксом prefix"""
    M=N if M== -1 else M
    prefix = prefix or []
    if M == 0:
        print(*prefix, end = ", ", sep="")
        return
    for number in range(1, N+1):
        if find(number, prefix):
            continue
        prefix.append(number)
        gen_permutations(N, M-1, prefix)
        # print(gen_permutations(N, M-1, prefix))
        prefix.pop()


# gen_permutations(3,3)

# a = [i for i in range(1,4)]
#
# for i in permutations(a):
#     print(*i, end=' ',sep ='')
# string = "How can mirrors be real if our eyes aren't real"
# def to_jaden_case(string):
#     str_a = string.replace("'",'')
#     fin = str_a.title()
#     return (fin[0:40]+"'"+fin[40:48])
#
#
#
#
# to_jaden_case(string)
# seconds = 359999
#
#
# def make_readable(s):
#     print('{:02}:{:02}:{:02}'.format(s / 3600, s / 60 % 60, s % 60))
#
# # make_readable(s)
#
# def make_readable(seconds):
#     hours, seconds = divmod(seconds, 60 ** 2)
#     minutes, seconds = divmod(seconds, 60)
#     print('{:02}:{:02}:{:02}'.format(hours, minutes, seconds))
#
# make_readable(seconds)

# arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -11, -12, -13, -14, -15]
# def count_positives_sum_negatives(arr):
#   output = []
#   if arr:
#     output.append(sum([1 for x in arr if x > 0]))
#     output.append(sum([x for x in arr if x < 0]))
#   print(output)
#
# count_positives_sum_negatives(arr)
# cap = 100
# on = 60
# wait = 50
#
#
# def enough(cap, on, wait):
#     cap_all = cap -(on+wait)
#     if 0 < cap_all <= cap:
#         print('0')
#     elif cap_all < 0:
#         print(cap_all*-1)
# (litres(2), 1, 'should return 1 litre')
#         test.assert_equals(litres(1.4), 0, 'should return 0 litres')
#         test.assert_equals(litres(12.3), 6, 'should return 6 litres')
#         test.assert_equals(litres(0.82), 0, 'should return 0 litres')
#         test.assert_equals(litres(11.8), 5, 'should return 5 litres')
#         test.assert_equals(litres(1787), 893, 'should return 893 litres')
#         test.assert_equals(litres(0), 0, 'should return 0 litres')

haystack = ['3', '123124234', None, 'needle', 'world', 'hay', 2, '3', True, False]
target = 'needle'
def find_needle(haystack):
    for i, j in enumerate(haystack):
        if j == target:
            print('found the needle at position','{}'.format(i))
#
# find_needle(haystack)

# song = ("AWUBBWUBC")

def song_decoder(song):
    r = "".join(set(song))
    print(r)
# c = 65/(1.75+1.75)
# print(c)
# song_decoder(song)
#
# a = int(input())
# b = float(input())
# index = a/(b+b)
# if 18.5 <= index < 25:
#     print('Оптимальная масса')
# elif int(index) < 18.5:
#     print('Недостаточная масса')
# elif index >= 25:
#     print('Избыточная масса')


#
# a = (input().split())
# double = []
# print(a)
# for i in a:
#     if i not in double:
#         double.append(i)
# print(len(double))


import requests
from bs4 import BeautifulSoup

response = requests.get("https://xakep.ru")
page = response.text

soup = BeautifulSoup(page, 'html.parser')

headings = map(lambda e: e.text, soup.select("h3.entry-title a span"))
for h in headings:
  print(h)
