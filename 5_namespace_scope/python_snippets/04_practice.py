# -*- coding: utf-8 -*-


# Ним — математическая игра, в которой два игрока по очереди берут предметы,
# разложенные на несколько кучек. За один ход может быть взято любое количество предметов
# (большее нуля) из одной кучки. Выигрывает игрок, взявший последний предмет.
# В классическом варианте игры число кучек равняется трём.

# Составить модуль, реализующий функциональность игры.
# Функции управления игрой
#  разложить_камни()
#  взять_из_кучи(NN, KK)
#  положение_камней() - возвращает список [XX, YY, ZZ, ...] - текущее расположение камней
#  есть_конец_игры() - возвращает True если больше ходов сделать нельзя
#
#
# В текущем модуле (5_namespace_scope/python_snippets/04_practice.py) реализовать логику работы с пользователем:
#  начало игры,
#  вывод расположения камней
#  ввод первым игроком хода - позицию и кол-во камней
#  вывод расположения камней
#  ввод вторым игроком хода - позицию и кол-во камней
#  вывод расположения камней

from nim_engine import put_stones, get_bunches, is_gameover, take_from_bunch
from termcolor import colored, cprint

put_stones()
user_number = 1
while True:
    cprint('Текущая позиция')
    print(get_bunches())
    user_color = 'blue' if user_number == 1 else 'yellow'
    print('Ход игрока {}'.format(user_number))
    pos = input('Откуда берем?')
    qua = input('Сколько берем?')
    step_successed = take_from_bunch(position=int(pos)), quantity=int(qua))
    if step_successed:
        user_number = 2 if user_number == 1 else 1
    else:
        print('Невозможный ход!')
    if is_gameover():
        break

print('Выйграл игрок номер {}'.format(user_number))

from nim_engine import put_stones, get_bunches, is_gameover, take_from_bunch

# put_stones()
# user_number = 1
# while True:
#     print('Текущая позиция')
#     print(get_bunches())
#     print('Ход игрока {}'.format(user_number))
#     pos = input('Откуда берем?')
#     qua = input('Сколько берем?')
#     step_successed = take_from_bunch (position=int(pos))
#     if step_successed:
#         user_number = 2 if
