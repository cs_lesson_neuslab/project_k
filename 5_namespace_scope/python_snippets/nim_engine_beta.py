from random import randint

_holder = []

def put_stones():
    global _holder
    for i in range(0 , randint(3, 5)):
        _holder.append(randint(20, 30))


def take_from_bunch(position, quantity):
    if 0 < position <= (len(_holder)):
        if quantity <= _holder[position-1]:
            _holder[position - 1] -= quantity
            return True
        else:
            return False
    else:
        return False


def get_bunches():
    return _holder


def game_over():
    return sum(_holder) == 0


