# Есть список песен группы Depeche Mode со временем звучания с точностью до долей минут
# Точность указывается в функции round(a, b)
# где a, это число которое надо округлить, а b количество знаков после запятой
# более подробно про функцию round смотрите в документации https://docs.python.org/3/search.html?q=round

violator_songs_list = [
    ['World in My Eyes', 4.86],
    ['Sweetest Perfection', 4.43],
    ['Personal Jesus', 4.56],
    ['Halo', 4.9],
    ['Waiting for the Night', 6.07],
    ['Enjoy the Silence', 4.20],
    ['Policy of Truth', 4.76],
    ['Blue Dress', 4.29],
    ['Clean', 5.83],
]

# распечатайте общее время звучания трех песен: 'Halo', 'Enjoy the Silence' и 'Clean' в формате
#   Три песни звучат ХХХ.XX минут
# Обратите внимание, что делать много вычислений внутри print() - плохой стиль.
# Лучше заранее вычислить необходимое, а затем в print(xxx, yyy, zzz)

#
a = violator_songs_list[3][1]
b = violator_songs_list[5][1]
c = violator_songs_list[8][1]
d = (round(a,1)+ round(b,1) + round(c,1))

#print('%.2f' % d)

print('Три песни звучат', ('%.2f' % d),'минут')

# Есть словарь песен группы Depeche Mode
violator_songs_dict = {
    'World in My Eyes': 4.76,
    'Sweetest Perfection': 4.43,
    'Personal Jesus': 4.56,
    'Halo': 4.30,
    'Waiting for the Night': 6.07,
    'Enjoy the Silence': 4.6,
    'Policy of Truth': 4.88,
    'Blue Dress': 4.18,
    'Clean': 5.68,
}

# распечатайте общее время звучания трех песен: 'Sweetest Perfection', 'Policy of Truth' и 'Blue Dress'
#   А другие три песни звучат ХХХ минут

#
a = (violator_songs_dict['Sweetest Perfection'])
b = (violator_songs_dict['Policy of Truth'])
c = (violator_songs_dict['Blue Dress'])
d = (a+b+c)
print('%.2f' % d)
a1 = (violator_songs_dict['Clean'])
b1 = (violator_songs_dict['Enjoy the Silence'])
c1 = (violator_songs_dict['Personal Jesus'])
d1 = (a1+b1+c1)
print('А другие три песни звучат', d1, 'минут')
# зачет