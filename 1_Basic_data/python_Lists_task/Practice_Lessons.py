# n = input("Введите целое число: ")
# a = input('Введите второе число:')
# try:
#     n == int(n)
#     a == int(a)
#     c = int(n) + int(a)
#     print(c)
# except:
#     print(n + a)

# old = int(input('Ваш возраст: '))
# print('Рекомендовано:', end=' ')
# if 3 <= old < 6:
#     print('"Заяц в лабиринте"')
# elif 6 <= old < 12:
#     print('"Марсианин"')
# elif 12 <= old < 16:
#     print('"Загадочный остров"')
# elif 16 <= old:
#     print('"Поток сознания"')

# a = int(input('Введите число:'))
# if a > 0:
#     print('1')
# elif a < 0:
#     print('-1')
# else:
#     print('0')

# n = input("Введите целое число: ")
# while type(n) != int:
#     try:
#         n = int(n)
#     except ValueError:
#         print("Неправильно ввели!")
#         n = input("Введите целое число: ")
# total = 100
# i = 4
# while i < 5:
#     n = int(input('Введите расход'))
#     total = total - n
#     if total < 0:
#         print('Операция не допустима')
#     break
#     i = i + 1
#     print("Осталось", total)
# a = 2
# while a < 2**20:
#     b = a ** 2
#     print(b)

# import PySimpleGUI as sg
# layout = [
#     [sg.Text('цена вход'), sg.InputText(), sg.FileBrowse(),
#      sg.Checkbox('MD5'), sg.Checkbox('SHA1')
#      ],
#     [sg.Text('цена продажа'), sg.InputText(), sg.FileBrowse(),
#      sg.Checkbox('SHA256')
#      ],
#     [sg.Output(size=(88, 20))],
#     [sg.Submit(), sg.Cancel()]
# ]
# window = sg.Window('File Compare', layout)
# while True:                             # The Event Loop
#     event, values = window.read()
#     # print(event, values) #debug
#     if event in (None, 'Exit', 'Cancel'):
#         break
#

# a=[x for x in range(1,100) if x % 3==0 or x % 5==0]
# print(sum(a))

# a = 1                   # задаем первые значения чисел Фибоначчи
# b = 2
# fibo = [1,2]            # создаем массив чисел Фибоначчи
# fibo2 = []              # создаем массив четных чисел фибоначчи
#
# while b < 4*10**6:
#     if a % 2 == 0:      # заполняем массив четных чисел фибоначчи
#         fibo2.append(a)
#     if b % 2 == 0:
#         fibo2.append(b)
#
#     a = a + b           # "третий" член ряда
#     b = a + b           # "четвертый" член
#     if a > 4*10**6:     # прерываем цикл, если значение а превысило 4 млн
#         break
#
#     fibo.append(a)      # переносим третий член в массив чисел фибоначчи
#     fibo.append(b)      # аналогично четвертый член
#
# print(fibo)             # вывод чисел фибоначчи до 4 млн
# print(fibo2)            # вывод четных чисел фибоначчи до 4 млн
# print(sum(fibo2))

# def fib(n):
#     cur = 1
#     old = 1
#     i = 1
#     while (i < n):
#         cur, old, i = cur+old, cur, i+1
#     return cur
#
# for i in range(100):
#     print(fib(i))
#
# fib(100)

# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# c = ()
# for i in a:
#     if i < 5:
#         print(i)

# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
#
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
# result = [elem for elem in a if elem in b]
#
# print(result)

# my_dict = {'a':20, 'b':5874, 'c': 560,'d':400, 'e':5874, 'f': 200}
# lis = [20, 5874, 560, 400, 5873, 200]
# max = lis[0]
# sec_max = lis[0]
# for i in lis:
#     if i > max:
#         sec_max = max
#         max = i
# print(sec_max)


# lis = [5, 7, 233, 8, 301, -1, 93, -4]
# max = lis[0]
# sec_max = lis[0]
# for i in lis:
#     if i > max:
#         sec_max = max
#         max = i
#
# print(sec_max)
# a = [2,4,6,7,9,10]
# max_num = a[0]
# sec_num = a[0]
#
# for i in a:
#
#     if i > max_num:
#         sec_num = max
#         max = i
# print(sec_num)
#
# Выведите числа от 1 до 100 с пропуском чисел 50 и 99.
#
# Создайте вывод при помощи цикла for, а также цикла while.

# for i in range(101):
#     if i == 50 or i == 99:
#         continue
#     print(i)

basket = [
    {
        'position': 'Book about python',
        'count': 1,
    },
    {
        'position': 'Mouse with cabel',
        'count': 12,
    }
]

count = 0
for element in basket:
    count += element['count']

print(count)